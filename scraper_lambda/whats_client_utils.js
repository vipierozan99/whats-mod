const { Message } = require("whatsapp-web.js");

const _fetchMessagesUptoDate = async (chatId, timestamp) => {
  const msgFilter = (m) => !m.isNotification; // dont include notification messages
  const chat = window.Store.Chat.get(chatId);
  let msgs = chat.msgs.models;
  let oldest_loaded_msg_ts = msgs.length > 0 ? msgs[0].__x_t : Date.now();

  while (oldest_loaded_msg_ts > timestamp) {
    const loadedMessages = await chat.loadEarlierMsgs();
    if (!loadedMessages) break;
    msgs = [...loadedMessages.filter(msgFilter), ...msgs];
    oldest_loaded_msg_ts = msgs[0].__x__t;
  }

  return msgs
    .filter((m) => m.__x_t >= timestamp)
    .map((m) => window.WWebJS.getMessageModel(m));
};

async function fetchMessagesUptoDate(chat, timestamp) {
  let messages = await chat.client.pupPage.evaluate(
    _fetchMessagesUptoDate,
    chat.id._serialized,
    timestamp
  );

  return messages.map((m) => new Message(chat.client, m));
}

module.exports = {
  fetchMessagesUptoDate,
};
