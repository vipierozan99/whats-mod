require("dotenv").config();
const chromium = require("chrome-aws-lambda");
const { Client } = require("whatsapp-web.js");
const { fetchMessagesUptoDate } = require("./whats_client_utils");

const lambdaHandler = async (event, context) => {
  console.log("HANDLER CALLED 2");
  const GROUP_ID = process.env.GROUP_ID;
  console.log(GROUP_ID);

  const client = new Client({
    // session: sessionExists() ? loadSession() : undefined,
    puppeteer: {
      executablePath: await chromium.executablePath,
      args: chromium.args,
    },
  });

  client.on("ready", async () => {
    console.log("Client is ready!");
    const week_ago = new Date();
    week_ago.setDate(week_ago.getDate() - 7);
    const week_ago_seconds = (week_ago.getTime() / 1000).toFixed();
    console.log(week_ago_seconds);

    const chat = await client.getChatById(chat_id._serialized);
    const messages = await fetchMessagesUptoDate(chat, week_ago_seconds);
    console.log(
      messages.map((m) => ({ body: m.body, timestamp: m.timestamp }))
    );
    await client.destroy();
  });

  await client.initialize();
};

module.exports.lambdaHandler = lambdaHandler;
