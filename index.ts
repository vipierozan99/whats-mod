import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as awsx from "@pulumi/awsx";

const image = awsx.ecr.buildAndPushImage("whats_scraper_image", {
  context: "./scraper_lambda",
});

const role = new aws.iam.Role("lambda_scraperRole", {
  assumeRolePolicy: aws.iam.assumeRolePolicyForPrincipal({
    Service: "lambda.amazonaws.com",
  }),
});
new aws.iam.RolePolicyAttachment("lambdaFullAccess", {
  role: role.name,
  policyArn: aws.iam.ManagedPolicy.AWSLambdaExecute,
});

const whats_scraper = new aws.lambda.Function("whats_scraper", {
  packageType: "Image",
  imageUri: image.imageValue,
  role: role.arn,
  timeout: 900,
  memorySize: 512,
});
