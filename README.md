# WHATS_MOD

A WhatsApp message scraper (for now).

Planned features:

- Weekly scrape of group chat
- Group analytics, like common topics and hot days
- Inactive member detection
- Automatic moderation (like reporting suspicious members)?
